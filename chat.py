#!/usr/bin/env python
from msg import *

intent_welcome = ['oi','olá','ola','olà','boa','noite','bom','tarde','dia','ai','chefe','cabeça','cabeca','hamburguer']
intent_wantburguer = ['quero', 'cardapio','opções','opcoes','opção ','hamburguer','hambúrguer','hambúrgueres','menu','cardápio','cardapio','preço','preços','preco','precos']
intent_cancel = ['deixa','desistir','desisti','desisto','cancela','cancelar','deixe']
intities_hamburguer = {'markbrew':['markbrew','markbrews','markbreu','markbreus','marbreu','marbreus','marbrew','marbrews','tradicional','tradicionais'],
    'doidimais':['doidimais','cupim','torresmo'],
    'humildao':['humildao','humildão','humildãos','humildaos','humilde','barato'],
    'sublime':['sublime','sublimes','novo'],
    'eggocentrico':['eggocentrico','eggocentricos','eggocêntrico','eggocêntricos','ovo']
}
intities_beverage = {'cerveja':['alternativa','alternativas','artesanal','artesanais','cerveja','cervejas'],
        'refrigerante':['refrigerante','refrigerantes','refri','refris','coca','cocas','coca-cola','coca-colas']
}

switch = {'WELCOME': 0,'MENU': 1,'GET_ORDER': 2,'GET_ADDRESS': 3,'CONFIRM_ORDER': 4}
STEP = 0
PART = 1

class Chat:
    def __init__(self):
        self.step = [0,0]
        self.order = {'markbrew':{'amount':0,'price':15},
        'doidimais':{'amount':0,'price':18},
        'humildao':{'amount':0,'price':10},
        'sublime':{'amount':0,'price':15},
        'eggocentrico':{'amount':0,'price':16},
        'cerveja':{'amount':0,'price': 7},
        'refrigerante':{'amount':0,'price':3}
        }
        self.username = None
        self.first_name = None
        self.last_name = None
        self.address = {'latitude':None, 'longitude':None, 'reference':None}
        self.previous_update = None
        self.lasttime = None


    def welcome(self,bot,update):
        self.previous_update = update.update_id
        phrase = update.message.text
        result = self.find_intent(phrase, intent_welcome)

		#bot.send_message(update.message.chat_id, text=result)
        if result == 1:
            bot.sendMessage(update.message.chat_id, text=msg_welcome, parse_mode='markdown')
            self.step[STEP]=switch['MENU']
        elif result == 0:
            update.message.reply_text("Você pode reformular sua afirmação? Eu não estou entendendo:")


    def menu(self, bot, update):
        self.previous_update = update.update_id
        phrase = update.message.text
        result = self.find_intent(phrase, intent_wantburguer)
        #update.message.reply_text("Dentro do menu " + str(update.update_id))

        if result == 1:
            bot.sendMessage(update.message.chat_id, text=msg_hamburguer, parse_mode='markdown')
            self.step[STEP] = switch['GET_ORDER']
        elif result == 0:
            update.message.reply_text("Você pode reformular sua afirmação? Eu não estou entendendo:")

    def order_beverage(self, bot, update):
        self.previous_update = update.update_id
        phrase = update.message.text


        phrase = phrase.lower()
        phrase = phrase.split()

        #update.message.reply_text("Update ID -> " + str(update.update_id))

        i = 0
        counter = 0
        while i < len(phrase):
            if phrase[i] in intities_beverage['cerveja']:
                try:
                    self.order['cerveja']['amount'] = int(phrase[i-1])
                except ValueError:
                        bot.sendMessage(update.message.chat_id, text=error_beverage, parse_mode='markdown')
                counter += 1
            if phrase[i] in intities_beverage['refrigerante']:
                try:
                    self.order['refrigerante']['amount'] = int(phrase[i-1])
                except ValueError:
                    bot.sendMessage(update.message.chat_id, text=error_beverage, parse_mode='markdown')
                counter += 1

            i+=1

        if counter == 0:
            if 'pular' in phrase:
                pass
            else:
                bot.sendMessage(update.message.chat_id, text=error_beverage, parse_mode='markdown')
                return

        self.show_order(bot,update)
        self.step[PART] += 1


    def get_name(self, bot, update):
        self.previous_update = update.update_id
        user = update.message.from_user
        self.username = user.username
        self.first_name = user.first_name
        self.last_name = user.first_name
        self.previous_update = update.update_id
        self.address['name'] = update.message.text
        self.step[PART] += 1

    def get_reference(self, bot, update):
        self.previous_update = update.update_id
        self.address['reference'] = update.message.text
        self.step[PART] += 1

    def confirm_order(self, bot, update):
        pass
        #acrescentar integração com banco de dados!


    def find_intent(self,phrase,intent_target):
        phrase = phrase.lower()
        phrase = phrase.split()
        list_results = [i for i in intent_target if i in phrase]
        if len(list_results)>0:
            return 1
        else:
            list_results  = [i for i in intent_cancel if i in phrase]
            if len(list_results)>0:
                return -1
            return 0

    def order_hamburguer(self, bot, update):
        """ Quando portar o codigo para o programa, ficar de olho no
        loop do programa principal e o argumento self"""
        self.previous_update = update.update_id
        phrase = update.message.text

        phrase = phrase.lower()
        phrase = phrase.split()

        #update.message.reply_text("Text -> " + str(update.message.text))

        i = 0
        counter = 0

        while i < len(phrase):
            if phrase[i] in intities_hamburguer['markbrew']:
                try:
                    self.order['markbrew']['amount'] = int(phrase[i-1])
                except ValueError:
                    bot.sendMessage(update.message.chat_id, text=error_hamburguer, parse_mode='markdown')
                    return
                counter += 1
            if phrase[i] in intities_hamburguer['doidimais']:
                try:
                    self.order['doidimais']['amount'] = int(phrase[i-1])
                except ValueError:
                    bot.sendMessage(update.message.chat_id, text=error_hamburguer, parse_mode='markdown')
                    return
                counter += 1

            if phrase[i] in intities_hamburguer['sublime']:
                try:
                    self.order['sublime']['amount'] = int(phrase[i-1])
                except ValueError:
                    bot.sendMessage(update.message.chat_id, text=error_hamburguer, parse_mode='markdown')
                    return
                counter += 1

            if phrase[i] in intities_hamburguer['eggocentrico']:
                try:
                    self.order['eggocentrico']['amount'] = int(phrase[i-1])
                except ValueError:
                    bot.sendMessage(update.message.chat_id, text=error_hamburguer, parse_mode='markdown')
                    return
                counter += 1

            if phrase[i] in intities_hamburguer['humildao']:
                try:
                    self.order['humildao']['amount'] = int(phrase[i-1])
                except ValueError:
                    bot.sendMessage(update.message.chat_id, text=error_hamburguer, parse_mode='markdown')
                    return
                counter += 1
            i+=1
        #self.show_order(bot,update)
        if counter == 0:
            bot.sendMessage(update.message.chat_id, text=error_hamburguer, parse_mode='markdown')
            return

        self.step[PART] += 1
        update.message.reply_text("Anotado!")

    def cancel(self, phrase):
        phrase = phrase.lower()
        phrase = phrase.split()
        list_results  = [i for i in intent_cancel if i in phrase]
        if len(list_results)>0:
            return -1
        return 0

    def clean_order(self):
        for i in self.order:
            self.order[i]['amount'] = 0

    def clean_address(self):
        for i in self.address:
            self.address[i] = None

    def show_order(self,bot,update):
        #keys = list(self.order.keys())
        amount = " "
        price = " "
        total = 0
        order = " "
        order +=  "*---- TOTAL DO PEDIDO ----*\n"
        for i in self.order:
            if self.order[i]['amount']>0:
                partial = self.order[i]['amount'] * self.order[i]['price']
                amount = str(self.order[i]['amount'])
                price = str(self.order[i]['price'])
                order +=  i.title()+' - '+amount+' x '+price+ ' = ' + str(partial)+'\n'
                total += partial
        order += 'Total -> R$ ' + str(total)
        bot.sendMessage(update.message.chat_id, text=order, parse_mode='markdown')

    def get_location(self, bot, update):
        self.address['latitude'] = float(update.message.location.latitude)
        self.address['longitude'] = float(update.message.location.longitude)
        update.message.reply_text("Digite algum ponto de referencia, número e/ou cor da casa e alguma outra observação que ajude a encontrar: ")
        self.step[PART]+=1

    def restart_order(self):
        self.step[STEP] = None
        self.step[PART] = None
        self.previous_update = None
        self.clean_order()
        self.clean_address()
        self.lasttime = None