#!/usr/bin/env python

from telegram.ext import Updater, MessageHandler, Filters, ConversationHandler
from chat import *
from msg import *
#from time import time
intent_yes= ['sim','s','yes']
intent_no= ['não','n','ñ','no']

def main(bot, update):

    #update.message.reply_text('Id Update -> '+ str(update.update_id))
    #update.message.reply_text('Text Update -> '+ update.message.text)
    #update.message.reply_text('Step  -> '+str(object_chat.step))

    if object_chat.step[STEP] == None:
        object_chat.step[STEP] = 0
        object_chat.step[PART] = 0
    # cancelar pedido
    if object_chat.cancel(update.message.text) == -1:
        update.message.reply_text('Okay! Obrigado e volte sempre!')
        object_chat.restart_order()


    if switch['WELCOME'] == object_chat.step[STEP]:
        object_chat.previous_update = update.update_id
        phrase = update.message.text
        result = object_chat.find_intent(phrase, intent_wantburguer)
        if result == 1:
            bot.sendMessage(update.message.chat_id, text=msg_hamburguer, parse_mode='markdown')
            object_chat.step[STEP] = switch['GET_ORDER']
            object_chat.step[PART] = 0
        else:
            object_chat.welcome(bot,update)

    #
    # Menu
    #

    if switch['MENU'] == object_chat.step[STEP]  and update.update_id > object_chat.previous_update:
        object_chat.menu(bot,update)
        # salva o id do update
        object_chat.previous_update = update.update_id

    #
    # Get order
    #

    if switch['GET_ORDER'] == object_chat.step[STEP] and update.update_id > object_chat.previous_update:

        # recebe dados do pedido de hambuguer
        if 0 == object_chat.step[PART] and update.update_id > object_chat.previous_update:
            object_chat.order_hamburguer(bot,update)
        # envia menu de bebidas
        if 1 == object_chat.step[PART]:
            bot.sendMessage(update.message.chat_id, text=msg_beverage, parse_mode='markdown')
            object_chat.step[PART] += 1
        # recebe dados sobre bebidas
        if 2 == object_chat.step[PART] and update.update_id > object_chat.previous_update:
            object_chat.order_beverage(bot,update)
        # envia mensagem para confirmação do pedido
        if 3 == object_chat.step[PART]:
            object_chat.step[PART]+=1
            update.message.reply_text("Confirmar dados de pedido (S)im ou (N)ão? " )
        # recebe dados de confirmação do pedido
        if 4 == object_chat.step[PART] and update.update_id > object_chat.previous_update:
            option = update.message.text
            option = option.lower()
            if option in intent_yes:
                object_chat.step[STEP]=switch['GET_ADDRESS']
                object_chat.step[PART]=0
            elif option in intent_no:
                bot.sendMessage(update.message.chat_id, text=msg_hamburguer, parse_mode='markdown')
                object_chat.clean_order()
                object_chat.step[PART]=0
            else:
                update.message.reply_text("Você pode reformular sua afirmação? Eu não estou entendendo:")
        # salva o id do update
        object_chat.previous_update = update.update_id
    #
    # Get address
    #

    # Obtendo dados para a entrega
    if switch['GET_ADDRESS'] == object_chat.step[STEP]:
        if 0 == object_chat.step[PART]:
            update.message.reply_text('Para a entrega precisamos de alguns dados do endereço. Por favor, envie sua localização.')
            object_chat.step[PART]+=1
        #referencia
        if 1 == object_chat.step[PART] and update.update_id > object_chat.previous_update:
            object_chat.get_reference(bot,update)

        #nome do cliente
        if 2 == object_chat.step[PART]:
            object_chat.previous_update = update.update_id
            update.message.reply_text("Digite o nome seu nome: ")
            object_chat.step[PART]+=1
        if 3 == object_chat.step[PART] and update.update_id > object_chat.previous_update:
            object_chat.get_name(bot,update)
        if 4 == object_chat.step[PART]:
            update.message.reply_text("Confirmar dados de entrega (S)im ou (N)ão? " )
            object_chat.step[PART]+=1
        if 5 == object_chat.step[PART] and update.update_id > object_chat.previous_update:
            option = update.message.text
            option = option.lower()
            if option in intent_yes:
                object_chat.step[STEP] = switch['CONFIRM_ORDER']
                object_chat.step[PART] = 0
            elif option in intent_no:
                update.message.reply_text('Para a entrega precisamos de alguns dados do endereço. Por favor, envie sua localização. ')
                object_chat.clean_address()
                object_chat.step[PART]=0
            else:
                update.message.reply_text("Você pode reformular sua afirmação? Eu não estou entendendo:")
        # salva o id do update
        object_chat.previous_update = update.update_id
    #
    # Get confirm order
    #

    if switch['CONFIRM_ORDER'] == object_chat.step[STEP]:
        if 0 == object_chat.step[PART]:
            object_chat.show_order(bot,update)
            update.message.reply_text('Confirmar envio de pedido (S)im ou (N)ão?')
            object_chat.step[PART]+=1
        if 1 == object_chat.step[PART] and update.update_id > object_chat.previous_update:
            option = update.message.text
            option = option.lower()
            if option in intent_yes:
                bot.sendMessage(update.message.chat_id, text="Pedido finalizado, obrigado!\nPor favor responda o questinário disponivel no link a seguir, para sabermos sua avaliação. https://goo.gl/forms/Bw9UMyhrCtx6JLTj1", parse_mode='html')
                object_chat.restart_order()
            elif option in intent_no:
                bot.sendMessage(update.message.chat_id, text=msg_hamburguer, parse_mode='markdown')
                object_chat.clean_address()
                object_chat.clean_order()
                object_chat.step[STEP]=switch['GET_ORDER']
            else:
                update.message.reply_text("Você pode reformular sua afirmação? Eu não estou entendendo:")
        object_chat.previous_update = update.update_id
    


object_chat = Chat()

updater = Updater('')

updater.dispatcher.add_handler(MessageHandler([Filters.text], main))
updater.dispatcher.add_handler(MessageHandler([Filters.location], object_chat.get_location))
updater.start_polling()
updater.idle()
