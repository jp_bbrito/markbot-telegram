msg_hamburguer = u""" ------------- Cardápio -------------
*Doidimais* é cupim 180g, queijo minas padrão, torresmo, ketchup de goiabada e picles de cebola roxa. R$ 18,00

*MarkBrew* é acém com peito bovino 180g, mussarela, bacon, cebola caramelizada e molho barbecue da casa. R$ 15,00

*Sublime* é cupim 180g, queijo minas padrão, bacon, cebola carameliza e ketchup de goiabada. R$ 21,00

*EGGocêntrico*  é acém com peito bovino 180, requeijão de corte, ovo caipira e tomate confit. R$ 16,00

*Humildão* é acém com peito bovino 180g e mussarela. R$ 10,00

Para fazer o pedido faça  como exemplificado abaixo, onde é coloca primeiro a quantidade e depois o produto correspondete!
'Eu quero 2 markbrew e 1 doidimais'
Digite seu pedido: """

msg_beverage = u"""Alguma bebida para acompanhar? Temos *Coca-cola* de 300ml por R$ 3,00 e cerveja artesanal *Alternativa* de 300ml que custo R$ 7,00! Caso não queira, digite *pular*."""

error_hamburguer ="""Não foi possivel entender sua escolha. Por favor digite novamente como exemplicado logo abaixo.
"Eu quero 5 *markbrew* e 3 *doidimais* ..."
Digite:
"""
error_beverage ="""Não foi possivel entender sua escolha. Por favor digite novamente como exemplicado logo abaixo.
"Eu quero 2 *coca-cola* e 1 *cerveja* ..."
Caso não queira alguma bebida digite *pular*. 
"""
msg_welcome ="Olá. Sou *MarkBot* seu atendente virtual, vou guiar você na solicitação de seu pedido.\
 Use palavras curtas para responder minhas perguntas. A nossa conversa está dividida em duas partes. A\
 primeira diz respeito aos produtos a serem pedidos e segunda ao dados do endereço de entrega. Para\
 desistir do pedido bastar digitar a palavra *cancelar* em qualquer lugar da conversa. Continuando\
 nosso papo, digite *menu* e será mostrado nossas opções de hambúrgueres."